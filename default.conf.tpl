server {
    listen ${LISTEN_PORT};

    location /static {
        alias /vol/static;
    }

    location / {
        uwsgi_pass              ${APP_HOST}:${APP_PORT};
        inlcude                 /ect/nginx/uwsgi_param;
        clinet_max_body_size    10M;

    }
}